# CHATET.in Expense Tracker

[![pipeline status](https://gitlab.com/chatetin/bot/badges/main/pipeline.svg)](https://gitlab.com/chatetin/bot/-/commits/main) [![coverage report](https://gitlab.com/chatetin/bot/badges/main/coverage.svg)](https://gitlab.com/chatetin/bot/-/commits/main)

CHATET.in is a Telegram Bot helping you to track your expenses!

## Setup

To run this bot, please make sure you have your Telegram bot token ready. Change the `BOT_USERNAME` in `BotConfig.java` and set `BOT_TOKEN` environment variable to your bot token.

## Contributors

CHATET.in is made by:

- Eko Julianto Salim
- Mardianto
- Mario Ekaputta
- Muhammad Dahlan Yasadipura
- Raul Arrafi Delfarra
