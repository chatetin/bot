package org.chatetin.export;

import org.chatetin.ChatetinBot;
import org.chatetin.model.Expense;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;

import java.io.*;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class CSVExporterTest {
    public static final long USER_ID = 1337L;
    public static final long CHAT_ID = 1337L;

    private DBContext db;
    private ChatetinBot bot;
    private MessageSender sender;
    private SilentSender silent;

    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
        sender = Mockito.mock(MessageSender.class);
        silent = Mockito.mock(SilentSender.class);
        bot.setSilentSender(silent);
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testCSVExporterCorrectly() throws Exception {
        List<Expense> dum = new ArrayList<>();
        dum.add(new Expense(1, "dum1", "dum1"));
        dum.add(new Expense(2, "dum2", "dum2"));

        Expense dum3 = new Expense(3,"dum3","dum3");
        LocalDate FebMonth = LocalDate.of(2021, Month.FEBRUARY,12);
        Timestamp setDifferentMonth = Timestamp.valueOf(FebMonth.atStartOfDay());
        dum3.setTimestamp(setDifferentMonth);
        dum.add(dum3);

        CSVExporter exportTest = new CSVExporter(dum, "june");
        exportTest.convertToCSV();

        File expected2 = new File("Report_june.csv");
        assertEquals(exportTest.getBulan(), "june");
        assertNotEquals(exportTest.getBulan(),"/export");
        assertEquals(exportTest.getBulan().split(" ").length, 1);
        assertFalse(exportTest.getListExpense().isEmpty());
        assertTrue((expected2.exists()));

        BufferedReader csvReader = new BufferedReader(new FileReader("Report_june.csv"));
        ArrayList<String> list = new ArrayList<>();
        String line = null;
        while ((line = csvReader.readLine()) != null) {
            list.add(line);
        }
        csvReader.close();
        assertEquals("value,notes,category", list.get(0));
        assertEquals("1,dum1,dum1", list.get(1));
        assertEquals("2,dum2,dum2", list.get(2));
        //dum3 is an expense in feb, not included in june report
        assertFalse(list.contains("3,dum3,dum3"));
    }

    @Test
    public void testCSVExporterWrongInput() throws Exception {
        List<Expense> dum = new ArrayList<>();
        dum.add(new Expense(1, "dum1", "dum1"));
        dum.add(new Expense(2, "dum2", "dum2"));

        CSVExporter exportFalse = new CSVExporter(dum, "asdfgh");

        assertEquals(exportFalse.getBulan(), "asdfgh");
        File notexpected = new File("Report_asdfgh.csv");
        assertFalse(notexpected.exists());
    }

    @Test
    public void throwsFileNotFoundExceptionWhenCreateCSV() throws FileNotFoundException {
        try {
            exportCSV exportcsv = new exportCSV();
            List<Expense> dum = new ArrayList<>();
            exportcsv.cetakreport(dum,null);

        } catch (Exception e) {
            assertEquals(e, FileNotFoundException.class);
            e.printStackTrace();
        }
    }

}

