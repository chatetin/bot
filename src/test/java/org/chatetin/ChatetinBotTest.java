package org.chatetin;

import org.chatetin.handlers.PaymentHandler;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.objects.MessageContext;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

import java.time.LocalDate;

import static org.mockito.Mockito.times;

public class ChatetinBotTest {
    public static final long USER_ID = 1337L;
    public static final long CHAT_ID = 1337L;


    private DBContext db;
    private ChatetinBot bot;
    private SilentSender silent;
    private PaymentHandler paymentHandler;
    private MessageSender sender;

    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
        silent = Mockito.mock(SilentSender.class);
        sender = Mockito.mock(MessageSender.class);
        bot.setSilentSender(silent);
        paymentHandler = new PaymentHandler(db, sender, silent);
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testReplyToHelp() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);

        bot.replyToHelp().action().accept(context);
        Mockito.verify(silent, times(1)).send("Under Construction", CHAT_ID);
    }

    @Test
    public void testHandleReport() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);

        bot.handleReport().action().accept(context);
        Mockito.verify(silent, times(1)).send("Report is Empty!", CHAT_ID);
    }

    @Test
    public void testHandlePaymentWhenDBIsNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);

        bot.handlePayment().action().accept(context);
        Mockito.verify(silent, times(1)).send("You don't have any due payment right now", CHAT_ID);
    }

    @Test
    public void testHandlePaymentDBIsNotNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);
        Message msg = new Message();
        Chat chat = new Chat();
        String payment = "10000 bla food "+ LocalDate.now();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);

        String replyMessage = "Your remaining payment this month: " +"\r\n"+ "1. ["+LocalDate.now()+"] bla, food, 10000"+"\r\n";
        bot.handlePayment().action().accept(context);
        Mockito.verify(silent, times(1)).send(replyMessage, CHAT_ID);
    }


    @Test
    public void testHandleAllPaymentWhenDBIsNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);

        bot.handleAllPayment().action().accept(context);
        Mockito.verify(silent, times(1)).send("You don't have any due payment right now", CHAT_ID);
    }

    @Test
    public void testHandleAllPaymentDBIsNotNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);
        Message msg = new Message();
        Chat chat = new Chat();
        LocalDate due = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue()+1, 1);
        String payment = "10000 bla food "+ due;

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);

        String replyMessage = "Your all payment: " +"\r\n"+ "1. ["+due+"] bla, food, 10000"+"\r\n";
        bot.handleAllPayment().action().accept(context);
        Mockito.verify(silent, times(1)).send(replyMessage, CHAT_ID);
    }

    @Test
    public void testHandleRecurringWhenDBisNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);

        bot.handleRecurring().action().accept(context);
        Mockito.verify(silent, times(1)).send("You don't have any recurring payment right now", CHAT_ID);
    }

    @Test
    public void testHandleRecurringWhenDBisNotNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        MessageContext context = MessageContext.newContext(upd, user, CHAT_ID, bot);

        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food 12");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataRecurring().accept(bot, upd);

        String replyMessage = "Your all recurring: " +"\r\n"+ "1. [Date 12] bla, food, 10000"+"\r\n";
        bot.handleRecurring().action().accept(context);
        Mockito.verify(silent, times(1)).send(replyMessage, CHAT_ID);
    }

}