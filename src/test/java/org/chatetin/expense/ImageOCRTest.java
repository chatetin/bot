package org.chatetin.expense;

import org.chatetin.ChatetinBot;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.telegrambots.meta.api.objects.*;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ImageOCRTest {
    private DBContext db;
    private ChatetinBot bot;

    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testGetValueFromOCRWith() throws Exception {
        Message msg = new Message();

        List<PhotoSize> lps = new LinkedList<>();
        PhotoSize ps = new PhotoSize();
        ps.setFileId("ngasal");
        ps.setFileSize(10090);
        lps.add(ps);
        msg.setPhoto(lps);

        ImageOCR imageOCR = new ImageOCR(bot);
        assertTrue(imageOCR.getValueFromOCR(msg).isEmpty());
    }

    @Test
    public void testGetValueFromOCRWSuccess() throws Exception {
        Message msg = new Message();

        List<PhotoSize> lps = new LinkedList<>();
        PhotoSize ps = new PhotoSize();
        ps.setFileId("AgACAgUAAxkBAAICcmCWaqbaoyNbetKblyqfkwABt6kQtQAC5KsxG8PdsVTDlEniLrLUabiUg290AAMBAAMCAANtAAMs2wQAAR8E");
        ps.setFileSize(10090);
        lps.add(ps);
        msg.setPhoto(lps);

        ImageOCR imageOCR = new ImageOCR(bot);
//        assertFalse(imageOCR.getValueFromOCR(msg).isEmpty());
    }

    @Test
    public void testGetValueFromOCRNoImage() throws Exception {
        Message msg = new Message();

        List<PhotoSize> lps = new LinkedList<>();
        msg.setPhoto(lps);

        ImageOCR imageOCR = new ImageOCR(bot);
        assertTrue(imageOCR.getValueFromOCR(msg).isEmpty());
    }

    @Test
    public void testGetValueFromOCRSuccess() throws Exception {
        Message msg = new Message();

        ImageOCR imageOCR = new ImageOCR(bot);

        Long v = imageOCR.getValueFromAPI("https://ekojsalim.github.io/adprotestimage.jpg").get();

        assertEquals(v, 315810);
    }

    @Test
    public void testGetValueFromOCRWFail() throws Exception {
        Message msg = new Message();

        ImageOCR imageOCR = new ImageOCR(bot);

        Long v = imageOCR.getValueFromAPI("https://ekojsalim.jpg").get();

        assertEquals(v, -1L);
    }
}