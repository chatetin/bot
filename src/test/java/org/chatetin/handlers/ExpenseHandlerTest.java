package org.chatetin.handlers;

import org.chatetin.ChatetinBot;
import org.chatetin.expense.ImageOCR;
import org.chatetin.model.Expense;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.objects.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

public class ExpenseHandlerTest {
    public static final long USER_ID = 1337L;
    public static final long CHAT_ID = 1337L;

    private DBContext db;
    private ChatetinBot bot;
    private MessageSender sender;
    private SilentSender silent;

    private ExpenseHandler expenseHandler;
    private Map<String, List<Expense>> expensesMap;

    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
        sender = Mockito.mock(MessageSender.class);
        silent = Mockito.mock(SilentSender.class);
        bot.setSilentSender(silent);

        expenseHandler = new ExpenseHandler(db, sender, silent, new ImageOCR(bot));
        expensesMap = db.getMap("EXPENSES");
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testIsNumericFailsWhenNull() {
        assertFalse(ExpenseHandler.isNumeric(null));
    }

    @Test
    public void testIsNumericFailsWhenNonNumeric() {
        assertFalse(ExpenseHandler.isNumeric("abcdefghi"));
        assertFalse(ExpenseHandler.isNumeric("12,98b"));
        assertFalse(ExpenseHandler.isNumeric("12fb7"));
    }

    @Test
    public void testIsNumericRecognizesNumeric() {
        assertTrue(ExpenseHandler.isNumeric("250.0"));
        assertTrue(ExpenseHandler.isNumeric("12"));
    }

    @Test
    public void testInitialAddFlow() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/add");
        msg.setFrom(user);
        upd.setMessage(msg);

        expenseHandler.addFlow().actOn(bot, upd);

        Mockito.verify(silent, times(1)).send("Please specify your expenses or send a photo!",
                CHAT_ID);
    }

    @Test
    public void testHandleTextData() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food");
        msg.setFrom(user);
        upd.setMessage(msg);

        expenseHandler.handleTextData().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Added bla!", CHAT_ID);
    }

    @Test
    public void testHandleImageData() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        List<PhotoSize> lps = new LinkedList<>();
        PhotoSize ps = new PhotoSize();
        ps.setFileId("AgACAgUAAxkBAAPsYLOw0m4gWys9F0X0Guos6D_neIEAAiSsMRvuZ6BVdVMyCCsah5xJibJudAADAQADAgADeAADb3AFAAEfBA");
        lps.add(ps);
        msg.setPhoto(lps);
        msg.setFrom(user);
        upd.setMessage(msg);

        expenseHandler.handleImageData().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("You can now either complete or override the expense data.\n" +
                "'[Value] [Description] [Category]' or just '[Description] [Category]'", CHAT_ID);
    }

    @Test
    public void testHandleImageDataWhenOCRComplete() {
        Optional<Long> val = Optional.of(318510L);


        expenseHandler.handleCompleteImageOCR(val, CHAT_ID, String.valueOf(USER_ID), new LinkedList<>());

        Mockito.verify(silent, times(1)).send("Your Receipt Parsed Value: 318510", CHAT_ID);
        Mockito.verify(silent, times(1)).send("You can now either complete or override the expense data.\n" +
                "'[Value] [Description] [Category]' or just '[Description] [Category]'", CHAT_ID);
    }

    @Test
    public void testHandleImageTextData() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        List<Expense> le = new LinkedList<>();
        le.add(new Expense(-1));
        expensesMap.put(String.valueOf(USER_ID), le);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food");
        msg.setFrom(user);
        upd.setMessage(msg);

        expenseHandler.handleImageTextData().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Added bla!", CHAT_ID);
    }

    @Test
    public void testHandleImageTextDataThenInvalidOverride() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        List<Expense> le = new LinkedList<>();
        le.add(new Expense(-1));
        expensesMap.put(String.valueOf(USER_ID), le);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000");
        msg.setFrom(user);
        upd.setMessage(msg);

        expenseHandler.handleImageTextData().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Invalid input! Please re-input your image and data!", CHAT_ID);
    }

    @Test
    public void testHandleImageTextDataThenDontOverride() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        List<Expense> le = new LinkedList<>();
        le.add(new Expense(-1));
        expensesMap.put(String.valueOf(USER_ID), le);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("bla food");
        msg.setFrom(user);
        upd.setMessage(msg);

        expenseHandler.handleImageTextData().accept(bot, upd);
        Expense newExpense = expensesMap.get(String.valueOf(USER_ID)).get(0);
        assertEquals(newExpense.getNotes(), "bla");
        assertEquals(newExpense.getCategory(), "food");
        Mockito.verify(silent, times(1)).send("Added bla!", CHAT_ID);
    }

    @Test
    public void testValidImageThenTextSuccess() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food");
        msg.setFrom(user);
        upd.setMessage(msg);

        assertTrue(expenseHandler.isValidTextResponseToImage(upd));
    }

    @Test
    public void testValidImageThenTextFailure() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000");
        msg.setFrom(user);
        upd.setMessage(msg);

        assertFalse(expenseHandler.isValidTextResponseToImage(upd));
    }
}