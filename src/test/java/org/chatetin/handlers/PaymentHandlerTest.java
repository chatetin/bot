package org.chatetin.handlers;

import org.chatetin.ChatetinBot;
import org.chatetin.payment.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.*;
import org.chatetin.payment.Constants;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.validation.constraints.AssertFalse;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

public class PaymentHandlerTest {
    public static final long USER_ID = 1337L;
    public static final long CHAT_ID = 1337L;

    private DBContext db;
    private ChatetinBot bot;
    private MessageSender sender;
    private SilentSender silent;

    private PaymentHandler paymentHandler;
    private Map<String, List<Payment>> paymentMap;
    private Map<String, List<String[]>> recurringMap;

    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
        sender = Mockito.mock(MessageSender.class);
        silent = Mockito.mock(SilentSender.class);
        bot.setSilentSender(silent);

        paymentHandler = new PaymentHandler(db, sender, silent);
        paymentMap = db.getMap("PAYMENT");
        recurringMap = db.getMap("RECURRING");
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testIsNumericFailsWhenNull() {
        assertFalse(PaymentHandler.isNumeric(null));
    }

    @Test
    public void testIsNumericFailsWhenNonNumeric() {
        assertFalse(PaymentHandler.isNumeric("abcdefghi"));
        assertFalse(PaymentHandler.isNumeric("12,98b"));
        assertFalse(PaymentHandler.isNumeric("12fb7"));
    }

    @Test
    public void testIsNumericRecognizesNumeric() {
        assertTrue(PaymentHandler.isNumeric("250.0"));
        assertTrue(PaymentHandler.isNumeric("12"));
    }

    @Test
    public void testIsValidDateFailsWhenNull() {
        assertFalse(PaymentHandler.isValidDate(null));
    }


    @Test
    public void testIsValidDateFailsWhenWrongFormat() {
        assertFalse(PaymentHandler.isValidDate("02-03-2020"));
        assertFalse(PaymentHandler.isValidDate("02-Maret-2020"));
        assertFalse(PaymentHandler.isValidDate("2021-Maret-20"));
    }

    @Test
    public void testIsValidDateRecognizesRightFormat() {
        assertTrue(PaymentHandler.isValidDate("2021-03-28"));
    }

    @Test
    public void testInitialAddReminder() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/addReminder");
        msg.setFrom(user);
        upd.setMessage(msg);

        paymentHandler.addReminder().actOn(bot, upd);
        Mockito.verify(silent, times(1)).send("Please specify your payment! [Value] [Description] [Category] [Date (format: YYYY-MM-DD)] Ex: 500000 Stock Investment 2021-12-12",
                CHAT_ID);
    }

    @Test
    public void testInitialAddRecurring() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/addRecurring");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.addRecurring().actOn(bot, upd);

        Mockito.verify(silent, times(1)).send("Please specify your recurring payment! [Value] [Description] [Category] [Date (format: DD)] Ex: 500000 Stock Investment 12 -> We will remind you every month on this day",
                CHAT_ID);
    }

    @Test
    public void testInitialStopRecurringWhenDBIsNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/stopRecurring");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.stopRecurring().actOn(bot, upd);

        Mockito.verify(silent, times(1)).send("You don't have any recurring payment right now",
                CHAT_ID);
    }

    @Test
    public void testInitialStopRecurringWhenDBNotNull() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food 12");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataRecurring().accept(bot, upd);

        msg.setText("/stopRecurring");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.stopRecurring().actOn(bot, upd);

        Mockito.verify(silent, times(1)).send("Please choose which number of your recurring payment below you want to delete!"+"\r\n"+"1. [Date 12] bla, food, 10000"+"\r\n",
                CHAT_ID);
    }

   @Test
   public void testHandleTextDataReminderWhenInputValid() {
       Update upd = new Update();
       User user = new User(USER_ID, "Anto", false);
       Message msg = new Message();
       Chat chat = new Chat();
       String payment = "10000 bla food "+LocalDate.now();

       chat.setId(CHAT_ID);
       msg.setChat(chat);
       msg.setText(payment);
       msg.setFrom(user);
       upd.setMessage(msg);
       paymentHandler.handleTextDataReminder().accept(bot, upd);

       assertTrue(paymentMap.get(String.valueOf(USER_ID)).size()==1);
       Mockito.verify(silent, times(1)).send("Added bla!", CHAT_ID);
   }

    @Test
    public void testHandleTextDataReminderWhenInputIsNotValid() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        String payment = "1a0000 bla food "+LocalDate.now();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Invalid input! Please re-input your payment information!", CHAT_ID);
    }


    @Test
    public void testHandleTextDataReminderWhenDueDateAlreadyPassed() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        String payment = "10000 bla food "+LocalDate.of(LocalDate.now().getYear()-1, LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth()+1);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Your due date is already passed", CHAT_ID);
    }

   @Test
   public void testHandleTextDataRecurringWhenInputIsValid() {
       Update upd = new Update();
       User user = new User(USER_ID, "Anto", false);
       Message msg = new Message();
       Chat chat = new Chat();

       chat.setId(CHAT_ID);
       msg.setChat(chat);
       msg.setText("10000 bla food 12");
       msg.setFrom(user);
       upd.setMessage(msg);
       paymentHandler.handleTextDataRecurring().accept(bot, upd);

       assertTrue(recurringMap.get(String.valueOf(USER_ID)).size()==1);
       Mockito.verify(silent, times(1)).send("Added bla!", CHAT_ID);
   }

    @Test
    public void testHandleTextDataRecurringWhenInputIsNotValid() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("1a0000 bla food 12");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataRecurring().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Invalid input! Please re-input your payment information!", CHAT_ID);
    }

    @Test
    public void testHandleTextDataStopRecurringWhenInputIsValid() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food 12");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataRecurring().accept(bot, upd);

        msg.setText("1");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataStopRecurring().accept(bot, upd);

        assertTrue(recurringMap.get(String.valueOf(USER_ID)).size()==0);
        Mockito.verify(silent, times(1)).send("Deleted! We won't remind you about this payment start from next month", CHAT_ID);
    }

    @Test
    public void testHandleTextDataStopRecurringWhenInputIsNotValid() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food 12");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataRecurring().accept(bot, upd);

        msg.setText("2");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataStopRecurring().accept(bot, upd);

        assertTrue(recurringMap.get(String.valueOf(USER_ID)).size()==1);
        Mockito.verify(silent, times(1)).send("Invalid input! Please re-input your payment information!", CHAT_ID);
    }

    @Test
    public void testSayReminderSendMessageWhenExist() throws TelegramApiException {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        String payment = "10000 bla food "+LocalDate.now();
        SendMessage sm = new SendMessage();
        sm.setText(Constants.TEXT_REMINDER);
        sm.setChatId(String.valueOf(CHAT_ID));

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);

        paymentHandler.sayReminder();
        Mockito.verify(sender, times(1)).execute(sm);
    }

    @Test
    public void testSayReminderNotSendMessageWhenNotExist() throws TelegramApiException {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        String payment = "10000 bla food "+LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth()+1);
        SendMessage sm = new SendMessage();
        sm.setText(Constants.TEXT_REMINDER);
        sm.setChatId(String.valueOf(CHAT_ID));

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);

        paymentHandler.sayReminder();
        Mockito.verify(sender, times(0)).execute(sm);
    }

    @Test
    public void testDoUpdateWorksAndSendMessage() throws TelegramApiException {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        SendMessage sm = new SendMessage();
        String payment = "10000 blo food "+LocalDate.now();
        sm.setText(Constants.TEXT_UPDATE);
        sm.setChatId(String.valueOf(CHAT_ID));

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000 bla food 1");
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataRecurring().accept(bot, upd);

        paymentHandler.updatePaymentMap();

        assertTrue(paymentMap.get(String.valueOf(USER_ID)).size()==2);
        Mockito.verify(sender, times(1)).execute(sm);
    }

    @Test
    public void testCheckPaymentReturnsFalseWhenNotExist(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        String payment = "10000 bla food "+LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonth(), LocalDate.now().getDayOfMonth()+1);
        SendMessage sm = new SendMessage();
        sm.setText(Constants.TEXT_REMINDER);
        sm.setChatId(String.valueOf(CHAT_ID));

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);

        assertFalse(paymentHandler.checkPayment(String.valueOf(CHAT_ID)));
    }

    @Test
    public void testCheckPaymentReturnsTrueWhenExist(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        String payment = "10000 bla food "+LocalDate.now();
        SendMessage sm = new SendMessage();
        sm.setText(Constants.TEXT_REMINDER);
        sm.setChatId(String.valueOf(CHAT_ID));

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText(payment);
        msg.setFrom(user);
        upd.setMessage(msg);
        paymentHandler.handleTextDataReminder().accept(bot, upd);

        assertTrue(paymentHandler.checkPayment(String.valueOf(CHAT_ID)));
    }
}