package org.chatetin.handlers;

import org.chatetin.ChatetinBot;
import org.chatetin.model.Expense;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.objects.*;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

public class ExportHandlerTest {
    public static final long USER_ID = 1337L;
    public static final long CHAT_ID = 1337L;

    private DBContext db;
    private ChatetinBot bot;
    private MessageSender sender;
    private SilentSender silent;

    private ExportHandler exportHandler;
    private Map<String, List<Expense>> expensesMap;

    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
        sender = Mockito.mock(MessageSender.class);
        silent = Mockito.mock(SilentSender.class);
        bot.setSilentSender(silent);

        exportHandler = new ExportHandler(db,sender,silent,bot);
        expensesMap = db.getMap("EXPENSES");

    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testExportInitialFlow() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/export");
        msg.setFrom(user);
        upd.setMessage(msg);

        exportHandler.replyToExport().actOn(bot, upd);
        assertEquals(msg.getText(), "/export");
        Mockito.verify(silent, times(1)).send("Please specify your expenses month report",
                CHAT_ID);
    }

    @Test
    public void testhandleExportperMonth(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("may");
        msg.setFrom(user);
        upd.setMessage(msg);

        exportHandler.handleExportperMonth().accept(bot, upd);

        assertEquals(msg.getText(), "may");
        assertNotEquals(msg.getText(),"/export");
        String[] listOfWord = msg.getText().split(" ");
        assertEquals(listOfWord.length, 1);

        Mockito.verify(silent, times(1)).send("expenses in may!", CHAT_ID);
    }

    @Test
    public void testhandleExportperMonthInvalid(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("asdfgh");
        msg.setFrom(user);
        upd.setMessage(msg);

        exportHandler.handleExportperMonth().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("asdfgh invalid month!", CHAT_ID);

    }

}