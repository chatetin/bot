package org.chatetin.handlers;

import org.chatetin.ChatetinBot;
import org.chatetin.budget.Budget;
import org.chatetin.expense.ImageOCR;
import org.chatetin.model.Expense;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.objects.*;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;

public class BudgetHandlerTest {
    public static final long USER_ID = 1337L;
    public static final long CHAT_ID = 1337L;

    private DBContext db;
    private ChatetinBot bot;
    private MessageSender sender;
    private SilentSender silent;

    private BudgetHandler budgetHandler;
    private Map<String, List<Expense>> expensesMap;
    private Map<String, List<Budget>> budgetMap;

    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
        sender = Mockito.mock(MessageSender.class);
        silent = Mockito.mock(SilentSender.class);
        bot.setSilentSender(silent);

        budgetHandler = new BudgetHandler(db, sender, silent, bot);
        expensesMap = db.getMap("EXPENSES");
        budgetMap = db.getMap("BUDGET");
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testInitialBudgetFlow() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/budget");
        msg.setFrom(user);
        upd.setMessage(msg);

        budgetHandler.budgetFlow().actOn(bot, upd);
        Mockito.verify(silent, times(1)).send("Please specify your budget!",
                CHAT_ID);
    }

    @Test
    public void testBudgetFlow() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        Budget budget = new Budget(10000);
        List<Budget> userBudget = budgetMap.getOrDefault("Anto", new LinkedList<>());
        userBudget.add(budget);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("10000");
        msg.setFrom(user);
        upd.setMessage(msg);

        budgetHandler.handleData().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("This month budget set for 10000",
                CHAT_ID);
    }



}
