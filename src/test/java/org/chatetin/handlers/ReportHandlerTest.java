package org.chatetin.handlers;

import org.chatetin.ChatetinBot;
import org.chatetin.expense.ImageOCR;
import org.chatetin.model.Expense;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.objects.*;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;

public class ReportHandlerTest {
    public static final long USER_ID = 1337L;
    public static final long CHAT_ID = 1337L;

    private DBContext db;
    private ChatetinBot bot;
    private MessageSender sender;
    private SilentSender silent;

    private ReportHandler reportHandler;
    private Map<String, List<Expense>> expensesMap;



    @BeforeEach
    public void setUp() throws Exception {
        db = MapDBContext.offlineInstance("test");

        bot = new ChatetinBot(db);
        bot.onRegister();
        sender = Mockito.mock(MessageSender.class);
        silent = Mockito.mock(SilentSender.class);
        bot.setSilentSender(silent);

        reportHandler = new ReportHandler(db, sender, silent, bot);
        expensesMap = db.getMap("EXPENSES");
    }

    @AfterEach
    public void tearDown() throws Exception {
        db.clear();
        db.close();
    }

    @Test
    public void testInitialReportFlow() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/report");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.replyToExport().actOn(bot, upd);


        Mockito.verify(silent, times(1)).send("Report is Empty! \n Please add the report first with command /add",
                CHAT_ID);
    }

    @Test
    public void testInitialReportFlowSuccess() {
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        Timestamp timestamp = new Timestamp(2021-06-06);

        Expense expense = new Expense(10000, "tes", "makan", timestamp);
        List<Expense> expenses = expensesMap.getOrDefault("Anto", new LinkedList<>());
        expenses.add(expense);

        String userIdString = String.valueOf(user.getId());

        expensesMap.put(userIdString,expenses);

        assertTrue(expensesMap.get(userIdString).size() == 1);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("/report");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.replyToExport().actOn(bot, upd);


        Mockito.verify(silent, times(1)).send("This is the category available in your report if you want to choose filter by Category: \n",
                CHAT_ID);
    }

    @Test
    public void testHandleReportFilterperMonth(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("january");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.handleReportFilter().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Pengeluaran untuk bulan january", CHAT_ID);
    }

    @Test
    public void testHandleReportFilterperDay(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("monday");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.handleReportFilter().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("your expenses on monday", CHAT_ID);
    }

    @Test
    public void testHandleReportFilterperCategory(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        Expense expense = new Expense(10000, "tes", "makan");
        List<Expense> expenses = expensesMap.getOrDefault("Anto", new LinkedList<>());
        expenses.add(expense);

        String userIdString = String.valueOf(user.getId());

        expensesMap.put(userIdString,expenses);

        assertTrue(expensesMap.get(userIdString).size() == 1);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("makan");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.handleReportFilter().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Pengeluaran untuk makan", CHAT_ID);
    }

    @Test
    public void testHandleReportFilterperDaySuccess(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        Timestamp timestamp = new Timestamp(2021-06-06);

        Expense expense = new Expense(10000, "tes", "makan", timestamp);
        List<Expense> expenses = expensesMap.getOrDefault("Anto", new LinkedList<>());
        expenses.add(expense);

        String userIdString = String.valueOf(user.getId());

        expensesMap.put(userIdString,expenses);

        assertTrue(expensesMap.get(userIdString)!=null);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("sunday");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.handleReportFilter().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("your expenses on sunday", CHAT_ID);
    }

    @Test
    public void testHandleReportFilterperMonthSuccess(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();
        Timestamp timestamp = new Timestamp(2021-06-06);

        Expense expense = new Expense(10000, "tes", "makan", timestamp);
        List<Expense> expenses = expensesMap.getOrDefault("Anto", new LinkedList<>());
        expenses.add(expense);

        String userIdString = String.valueOf(user.getId());

        expensesMap.put(userIdString,expenses);

        assertTrue(expensesMap.get(userIdString).size() == 1);

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("june");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.handleReportFilter().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("Pengeluaran untuk bulan june", CHAT_ID);
    }

    @Test
    public void testHandleReportFilterReject(){
        Update upd = new Update();
        User user = new User(USER_ID, "Anto", false);
        Message msg = new Message();
        Chat chat = new Chat();

        chat.setId(CHAT_ID);
        msg.setChat(chat);
        msg.setText("abc");
        msg.setFrom(user);
        upd.setMessage(msg);

        reportHandler.handleReportFilter().accept(bot, upd);
        Mockito.verify(silent, times(1)).send("abc invalid filter!", CHAT_ID);
    }


}
