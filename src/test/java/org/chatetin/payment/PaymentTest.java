package org.chatetin.payment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PaymentTest {
    private Class<?> paymentClass;
    private Payment payment;

    @BeforeEach
    public void setUp() throws Exception {
        paymentClass = Class.forName("org.chatetin.payment.Payment");
        payment = new Payment();
    }

    @Test
    public void testPaymentHasValueSetter() throws Exception {
        Method setValue = paymentClass.getDeclaredMethod("setValue", long.class);

        assertTrue(Modifier.isPublic(setValue.getModifiers()));
        assertEquals(1, setValue.getParameterCount());
        assertEquals("void", setValue.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPaymentHasValueGetter() throws Exception {
        Method getValue = paymentClass.getDeclaredMethod("getValue");

        assertTrue(Modifier.isPublic(getValue.getModifiers()));
        assertEquals(0, getValue.getParameterCount());
        assertEquals(long.class, getValue.getGenericReturnType());
    }

    @Test
    public void testPaymentValueSetterWorks() throws Exception {
        payment.setValue(20000);
        assertEquals(20000, payment.getValue());
    }

    @Test
    public void testPaymentHasNotesSetter() throws Exception {
        Method setNotes = paymentClass.getDeclaredMethod("setNotes", String.class);

        assertTrue(Modifier.isPublic(setNotes.getModifiers()));
        assertEquals(1, setNotes.getParameterCount());
        assertEquals("void", setNotes.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPaymentHasNotesGetter() throws Exception {
        Method getNotes = paymentClass.getDeclaredMethod("getNotes");

        assertTrue(Modifier.isPublic(getNotes.getModifiers()));
        assertEquals(0, getNotes.getParameterCount());
        assertEquals(String.class, getNotes.getGenericReturnType());
    }

    @Test
    public void testPaymentNotesSetterWorks() throws Exception {
        payment.setNotes("Makanan");
        assertEquals("Makanan", payment.getNotes());
    }

    @Test
    public void testPaymentHasCategorySetter() throws Exception {
        Method setCategory = paymentClass.getDeclaredMethod("setCategory", String.class);

        assertTrue(Modifier.isPublic(setCategory.getModifiers()));
        assertEquals(1, setCategory.getParameterCount());
        assertEquals("void", setCategory.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPaymentHasCategoryGetter() throws Exception {
        Method getCategory = paymentClass.getDeclaredMethod("getCategory");

        assertTrue(Modifier.isPublic(getCategory.getModifiers()));
        assertEquals(0, getCategory.getParameterCount());
        assertEquals(String.class, getCategory.getGenericReturnType());
    }

    @Test
    public void testPaymentCategorySetterWorks() throws Exception {
        payment.setCategory("Makanan");
        assertEquals("Makanan", payment.getCategory());
    }

    @Test
    public void testPaymentHasDueSetter() throws Exception {
        Method setDue = paymentClass.getDeclaredMethod("setDue", LocalDate.class);

        assertTrue(Modifier.isPublic(setDue.getModifiers()));
        assertEquals(1, setDue.getParameterCount());
        assertEquals("void", setDue.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPaymentHasDueGetter() throws Exception {
        Method getDue = paymentClass.getDeclaredMethod("getDue");

        assertTrue(Modifier.isPublic(getDue.getModifiers()));
        assertEquals(0, getDue.getParameterCount());
        assertEquals(LocalDate.class, getDue.getGenericReturnType());
    }

    @Test
    public void testPaymentDateSetterWorks() throws Exception {
        payment.setDue(LocalDate.of(2021, 12, 12));
        assertEquals(LocalDate.of(2021, 12, 12), payment.getDue());
    }
}