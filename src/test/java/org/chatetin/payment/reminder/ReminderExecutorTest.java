package org.chatetin.payment.reminder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;

public class ReminderExecutorTest {
    private Class<?> reminderExecutorClass;
    private ReminderExecutor reminderExecutor;

    @BeforeEach
    public void setUp() throws Exception {
        reminderExecutorClass = Class.forName("org.chatetin.payment.reminder.ReminderExecutor");
        reminderExecutor = mock(ReminderExecutor.class);
    }

    @Test
    public void testPaymentHasStartExecutionAtMethod() throws Exception {
        Method startExecutionAt = reminderExecutorClass.getDeclaredMethod("startExecutionAt", int.class, int.class, int.class);

        assertTrue(Modifier.isPublic(startExecutionAt.getModifiers()));
        assertEquals(3, startExecutionAt.getParameterCount());
        assertEquals("void", startExecutionAt.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPaymentHasStartUpdateAtMethod() throws Exception {
        Method startUpdateAt = reminderExecutorClass.getDeclaredMethod("startUpdateAt", int.class, int.class, int.class);

        assertTrue(Modifier.isPublic(startUpdateAt.getModifiers()));
        assertEquals(3, startUpdateAt.getParameterCount());
        assertEquals("void", startUpdateAt.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPaymentHasComputeNextDelayMethod() throws Exception {
        Method computeNextDelay = reminderExecutorClass.getDeclaredMethod("computeNextDelay", int.class, int.class, int.class);

        assertTrue(Modifier.isPrivate(computeNextDelay.getModifiers()));
        assertEquals(3, computeNextDelay.getParameterCount());
        assertEquals("long", computeNextDelay.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPaymentHasStopMethod() throws Exception {
        Method stop = reminderExecutorClass.getDeclaredMethod("stop");

        assertTrue(Modifier.isPublic(stop.getModifiers()));
        assertEquals(0, stop.getParameterCount());
        assertEquals("void", stop.getGenericReturnType().getTypeName());
    }
}