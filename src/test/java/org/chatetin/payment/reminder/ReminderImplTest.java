package org.chatetin.payment.reminder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReminderImplTest {
    private Class<?> reminderClass;
    private Class<?> reminderImplClass;
    private Class<?> callBackClass;
    private ReminderImpl reminder;
    private ReminderImpl.Callback callback;

    @BeforeEach
    public void setUp() throws Exception {
        reminderClass = Class.forName("org.chatetin.payment.reminder.Reminder");
        reminderImplClass = Class.forName("org.chatetin.payment.reminder.ReminderImpl");
        callBackClass = ReminderImpl.Callback.class;
        callback = Mockito.mock(ReminderImpl.Callback.class);
        reminder = new ReminderImpl(callback);
    }

    @Test
    public void testReminderIsAPublicInterface() {
        int classModifiers = reminderClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testCallbackIsAPublicInterface() {
        int classModifiers = callBackClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testPaymentHasExecuteReminderMethod() throws Exception {
        Method executeReminder = reminderImplClass.getDeclaredMethod("executeReminder");

        assertTrue(Modifier.isPublic(executeReminder.getModifiers()));
        assertEquals(0, executeReminder.getParameterCount());
        assertEquals("void", executeReminder.getGenericReturnType().getTypeName());
    }

    @Test
    public void testExecuteReminderWorks() throws Exception {
        reminder.executeReminder();
        Mockito.verify(callback, Mockito.times(1)).onTimeForReminder();
    }

    @Test
    public void testPaymentHasExecuteUpdateMethod() throws Exception {
        Method executeUpdate = reminderImplClass.getDeclaredMethod("executeUpdate");

        assertTrue(Modifier.isPublic(executeUpdate.getModifiers()));
        assertEquals(0, executeUpdate.getParameterCount());
        assertEquals("void", executeUpdate.getGenericReturnType().getTypeName());
    }

    @Test
    public void testExecuteUpdateWorks() throws Exception {
        reminder.executeUpdate();
        Mockito.verify(callback, Mockito.times(1)).onTimeForUpdate();
    }

}