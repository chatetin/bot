package org.chatetin.payment;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Modifier;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConstantsTest {
    private Class<?> constantClass;

    @BeforeEach
    public void setUp() throws Exception {
        constantClass = Class.forName("org.chatetin.payment.Constants");
    }

    @Test
    public void testConstantsIsAPublicInterface() {
        int classModifiers = constantClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }
}