package org.chatetin.budget;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BudgetTest {
    private Class<?> budgetClass;
    private Budget budget;

    @BeforeEach
    public void setUp() throws Exception {
        budgetClass = Class.forName("org.chatetin.budget.Budget");
        budget = new Budget(10000);
    }

    @Test
    public void testBudgetHasMaxExpanseGetter() throws Exception {
        Method getMaxExpense = budgetClass.getDeclaredMethod("getMaxExpense");

        assertTrue(Modifier.isPublic(getMaxExpense.getModifiers()));
        assertEquals(0, getMaxExpense.getParameterCount());
        assertEquals(long.class, getMaxExpense.getGenericReturnType());
    }
}