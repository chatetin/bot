package org.chatetin.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Getter
@Setter
public class Expense implements Serializable {
    private long value;
    private String notes;
    private String category;
    private Timestamp timestamp;

    public Expense(long value, String notes, String category) {
        this.value = value;
        this.notes = notes;
        this.category = category;
        this.timestamp = Timestamp.from(Instant.now());
    }

    public Expense(long value, String notes, String category, Timestamp timestamp) {
        this.value = value;
        this.notes = notes;
        this.category = category;
        this.timestamp = Timestamp.from(Instant.now());
    }

    public Expense(long value) {
        this(value, "", "");
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String getMonth(){
        int month = timestamp.getMonth();
        return checkMonth(month);
    }

    public String getDay(){
        int day = timestamp.getDay();
        return checkDay(day);
    }

    public String checkDay(int day){
        String[] dayName = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
        return dayName[day];
    }

    public String checkMonth(int month){
        String[] monthNames = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
        return monthNames[month];
    }

    public String toString(){
        return "Category: " + getCategory() + "\n" +
                "Value: " + getValue() + "\n" +
                "Day: " + getDay() + "\n" +
                "Month: " + getMonth() + "\n" +
                "Notes: "  + getNotes();
    }

}
