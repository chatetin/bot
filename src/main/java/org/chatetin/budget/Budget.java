package org.chatetin.budget;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class Budget implements Serializable {
    private long maxExpense;

    public Budget(long maxExpense) {
        this.maxExpense = maxExpense;
    }

    public long getMaxExpense() {
        return maxExpense;
    }

}
