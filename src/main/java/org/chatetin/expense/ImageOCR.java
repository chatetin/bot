package org.chatetin.expense;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;
import org.chatetin.BotConfig;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;


public class ImageOCR {
    private AbilityBot bot;

    public ImageOCR(AbilityBot bot) {
        this.bot = bot;
    }

    public Optional<CompletableFuture<Long>> getValueFromOCR(Message msg) {
        return Optional.ofNullable(getPhoto(msg))
                .map(this::getFilePath)
                .map(this::getValueFromAPI);
    }

    private PhotoSize getPhoto(Message msg) {
        if (msg.hasPhoto()) {
            List<PhotoSize> photos = msg.getPhoto();
            return photos.stream().max(Comparator.comparing(PhotoSize::getFileSize)).orElse(null);
        }
        return null;
    }

    private String getFilePath(PhotoSize photo) {
        GetFile getFileMethod = new GetFile();
        getFileMethod.setFileId(photo.getFileId());
        try {
            org.telegram.telegrambots.meta.api.objects.File file = bot.execute(getFileMethod);
            return file.getFileUrl(System.getenv("BOT_TOKEN"));
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

        return null;
    }

    public CompletableFuture<Long> getValueFromAPI(String filePath) {
        AsyncHttpClient client = Dsl.asyncHttpClient();
        return client.prepareGet(BotConfig.getOCRAPIUrl() + "/ocr?url=" + filePath)
                .execute()
                .toCompletableFuture()
                .handle((result, ex) -> Long.parseLong(result.getResponseBody()));
    }
}
