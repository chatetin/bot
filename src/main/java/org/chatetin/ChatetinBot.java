package org.chatetin;

import org.chatetin.expense.ImageOCR;
import org.chatetin.export.CSVExporter;
import org.chatetin.handlers.ExpenseHandler;
import org.chatetin.handlers.BudgetHandler;
import org.chatetin.handlers.ExportHandler;
import org.chatetin.handlers.PaymentHandler;
import org.chatetin.handlers.ReportHandler;
import org.chatetin.model.Expense;
import org.chatetin.payment.Payment;
import org.chatetin.payment.reminder.ReminderImpl;
import org.chatetin.payment.reminder.ReminderExecutor;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.db.MapDBContext;
import org.telegram.abilitybots.api.objects.*;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Predicate;

public class ChatetinBot extends AbilityBot implements ReminderImpl.Callback {

    private final PaymentHandler paymentHandler;
    private final ReminderExecutor reminderExecutor;

    public ChatetinBot() {
        this(MapDBContext.onlineInstance(BotConfig.BOT_USERNAME));
    }

    public ChatetinBot(DBContext db) {
        super(BotConfig.getBotToken(), BotConfig.BOT_USERNAME, db);
        db.getMap("EXPENSES").clear();
        paymentHandler = new PaymentHandler(db, sender, silent);
        reminderExecutor = new ReminderExecutor(new ReminderImpl(this));
        reminderExecutor.startExecutionAt(9, 0, 0);
        if (LocalDate.now().getDayOfMonth() == 1) {
            reminderExecutor.startUpdateAt(8, 30, 0);
        }
    }

    public Ability replyToHelp() {
        return Ability
                .builder()
                .name("help")
                .info("Bot Help")
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> silent.send("Under Construction", ctx.chatId()))
                .build();
    }

    public Ability handleReport() {
        return Ability.builder()
                .name("expensereport")
                .info("Expenses Report")
                .privacy(Privacy.PUBLIC)
                .locality(Locality.ALL)
                .input(0)
                .action(ctx -> {
                    Map<String, List<Expense>> expensesMap = db.getMap("EXPENSES");
                    long userId = ctx.user().getId();
                    String userIdString = String.valueOf(userId);
                    System.out.println(userIdString);
                    System.out.println(expensesMap.get(userIdString));
                    if (expensesMap.containsKey(userIdString)) {
                        System.out.println(expensesMap.get(userIdString).size());
                        silent.send("number of expense reports on the list: " + (expensesMap.get(userIdString).size()), ctx.chatId());
                        silent.send("Choose filter for your report expense: \n" +
                                            "Category \n" + "Month \n" + "Day \n ", ctx.chatId());
                        for (Expense expense : expensesMap.get(userIdString)) {
                            silent.send(expense.toString(), ctx.chatId());
                        }
                    } else {
                        silent.send("Report is Empty!", ctx.chatId());
                    }
                })
                .build();
    }

    private Predicate<Update> hasMessageWith(String msg) {
        return upd -> upd.getMessage().getText().equalsIgnoreCase(msg);
    }

    public void test() throws TelegramApiException {
        SendDocument sendDocument = new SendDocument();
        InputFile doc = new InputFile();
        sendDocument.setDocument(doc);
        this.execute(sendDocument);

        SendPhoto sendPhoto = new SendPhoto();
        InputFile photo = new InputFile();
        sendPhoto.setPhoto(photo);
        this.execute(sendPhoto);
    }

    public Ability handleExport() {
        return Ability.builder()
                .name("exportreport")
                .info("Export Report")
                .privacy(Privacy.PUBLIC)
                .locality(Locality.ALL)
                .input(1)
                .action(ctx -> {
                    Map<String, List<Expense>> expensesMap = db.getMap("EXPENSES");
                    long userId = ctx.user().getId();
                    String userIdString = String.valueOf(userId);
                    System.out.println(userIdString);
                    System.out.println(expensesMap.get(userIdString));
                }).build();
    }

    public Ability handleState() {
        return Ability.builder()
                .name("state")
                .info("Get User State")
                .privacy(Privacy.PUBLIC)
                .locality(Locality.ALL)
                .input(0)
                .action(ctx -> {
                    Map<Long, Integer> stateMap = db.getMap("user_state_replies");
                    System.out.println(stateMap.getOrDefault(ctx.chatId(), -1));
                    silent.send(String.valueOf(stateMap.getOrDefault(ctx.chatId(), -1)), ctx.chatId());
                })
                .build();
    }

    public AbilityExtension ExpenseAbilities() {
        return new ExpenseHandler(db, sender, silent, new ImageOCR(this));
    }

    public AbilityExtension ExportAbilities() {
        return new ExportHandler(db, sender, silent, this);
    }

    public AbilityExtension ReportAbilities() {
        return new ReportHandler(db, sender, silent, this);
    }

    public Ability handlePayment() {
        return Ability
                .builder()
                .name("reminder")
                .info("Payment")
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> {
                    Map<String, List<Payment>> paymentMap = db.getMap("PAYMENT");
                    long userId = ctx.user().getId();
                    String userIdString = String.valueOf(userId);
                    System.out.println(userIdString);
                    System.out.println(paymentMap.get(userIdString));
                    if (paymentMap.containsKey(userIdString)) {
                        String textReply = "Your remaining payment this month: " + "\r\n";
                        int idx = 1;
                        for (Payment pay : paymentMap.get(userIdString)) {
                            if (pay.getDue().compareTo(LocalDate.now()) >= 0 && pay.getDue().getMonth() == LocalDate.now().getMonth()) {
                                textReply += idx + ". [" + pay.getDue() + "] " + pay.getNotes() + ", " + pay.getCategory() + ", " + pay.getValue() + "\r\n";
                                idx++;
                            } else if (pay.getDue().compareTo(LocalDate.now()) < 0 || pay.getDue().getMonth() != LocalDate.now().getMonth()) {
                                continue;
                            } else {
                                silent.send("You don't have any due payment right now", ctx.chatId());
                            }
                        }
                        System.out.println(paymentMap.get(userIdString).size());
                        silent.send(textReply, ctx.chatId());
                    } else {
                        silent.send("You don't have any due payment right now", ctx.chatId());
                    }
                })
                .build();
    }

    public Ability handleAllPayment() {
        return Ability
                .builder()
                .name("reminderall")
                .info("AllPayment")
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> {
                    Map<String, List<Payment>> paymentMap = db.getMap("PAYMENT");
                    long userId = ctx.user().getId();
                    String userIdString = String.valueOf(userId);
                    System.out.println(userIdString);
                    System.out.println(paymentMap.get(userIdString));
                    if (paymentMap.containsKey(userIdString)) {
                        String textReply = "Your all payment: " + "\r\n";
                        int idx = 1;
                        for (Payment pay : paymentMap.get(userIdString)) {
                            if (pay.getDue().compareTo(LocalDate.now()) >= 0) {
                                textReply += idx + ". [" + pay.getDue() + "] " + pay.getNotes() + ", " + pay.getCategory() + ", " + pay.getValue() + "\r\n";
                                idx++;
                            }
                        }
                        if(paymentMap.get(userIdString).size()==0){
                            silent.send("You don't have any due payment right now", ctx.chatId());
                        }
                        System.out.println(paymentMap.get(userIdString).size());
                        silent.send(textReply, ctx.chatId());
                    } else {
                        silent.send("You don't have any due payment right now", ctx.chatId());
                    }
                })
                .build();
    }

    public Ability handleRecurring() {
        return Ability
                .builder()
                .name("recurring")
                .info("AllRecurringPayment")
                .locality(Locality.ALL)
                .privacy(Privacy.PUBLIC)
                .action(ctx -> {
                    Map<String, List<String[]>> recurringMap = db.getMap("RECURRING");
                    long userId = ctx.user().getId();
                    String userIdString = String.valueOf(userId);
                    System.out.println(userIdString);
                    System.out.println(recurringMap.get(userIdString));
                    if (recurringMap.containsKey(userIdString)) {
                        String textReply = "Your all recurring: "+"\r\n";
                        int idx = 1;
                        for(String[] recurring : recurringMap.get(userIdString)){
                            textReply+= idx+". [Date "+recurring[3]+"] "+recurring[1]+ ", "+recurring[2]+", "+recurring[0]+"\r\n";
                            idx++;
                        }
                        if(recurringMap.get(userIdString).size()==0){
                            silent.send("You don't have any recurring payment right now", ctx.chatId());
                        }
                        System.out.println(recurringMap.get(userIdString).size());
                        silent.send(textReply, ctx.chatId());
                    } else {
                        silent.send("You don't have any recurring payment right now", ctx.chatId());
                    }
                })
                .build();
    }

    public AbilityExtension PaymentAbilities() {
        return new PaymentHandler(db, sender, silent);
    }

    public AbilityExtension BudgetAbilities() {
        return new BudgetHandler(db, sender,silent, this);
    }

    // For testing purposes
    // working around library limitations
    // issue: https://github.com/rubenlagus/TelegramBots/issues/723
    public void setSender(MessageSender sender) {
        this.sender = sender;
    }

    public void setSilentSender(SilentSender sender) {
        this.silent = sender;
    }

    @Override
    public void onTimeForReminder() {
        paymentHandler.sayReminder();
    }

    @Override
    public void onTimeForUpdate() {
        paymentHandler.updatePaymentMap();
    }

    @Override
    public long creatorId() {
//        return 1381713377; // anto
        return 1107834700; //mario
    }
}
