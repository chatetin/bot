package org.chatetin.export;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.Response;
import java.util.concurrent.CompletableFuture;

import java.io.*;
import java.util.*;
import java.util.concurrent.Future;

import lombok.Getter;
import org.chatetin.model.*;


public class CSVExporter{

    exportCSV export;
    @Getter
    List<Expense> listExpense;
    @Getter
    String bulan;

    public CSVExporter(List<Expense> listExpense, String bulan){
        this.export = new exportCSV();
        this.listExpense = listExpense;
        this.bulan = bulan;
    }

    public CompletableFuture<Void> convertToCSV(){
        export.cetakreport(listExpense, bulan);
        return null;
    }
}