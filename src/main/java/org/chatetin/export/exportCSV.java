package org.chatetin.export;

import java.io.*;
import org.chatetin.model.*;
import java.util.Calendar;
import java.util.List;

public class exportCSV{

    public void cetakreport(List<Expense> listExpense, String bulan){

        String[] monthNames = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
            try {
                PrintWriter pw = new PrintWriter(new File(String.format("Report_%s.csv",bulan)));
                StringBuilder sb = new StringBuilder();
                sb.append("value");
                sb.append(",");
                sb.append("notes");
                sb.append(",");
                sb.append("category");
                sb.append("\n");

                Calendar cal = Calendar.getInstance();

                for(Expense ex : listExpense){
                    long timestamp = ex.getTimestamp().getTime();
                    cal.setTimeInMillis(timestamp);
                    int month = cal.get(Calendar.MONTH);
                    if((monthNames[month]).equals(bulan)){
                        sb.append(ex.getValue());
                        sb.append(",");
                        sb.append(ex.getNotes());
                        sb.append(",");
                        sb.append(ex.getCategory());
                        sb.append("\n");
                    }
                }

                pw.write(sb.toString());
                pw.close();

            } catch(FileNotFoundException e) { e.printStackTrace(); }
    }
}