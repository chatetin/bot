package org.chatetin.handlers;

import org.chatetin.budget.Budget;
import org.chatetin.model.Expense;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.abilitybots.api.objects.*;
import org.telegram.abilitybots.api.objects.Reply;
import org.telegram.abilitybots.api.objects.ReplyFlow;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class BudgetHandler implements AbilityExtension {
    // DB and sender
    protected final DBContext db;
    protected MessageSender sender;
    protected SilentSender silent;
    private Map<String, List<Budget>> budgetMap;
    private Map<String, List<Expense>> expensesMap;
    protected  AbilityBot bot;

    public BudgetHandler(DBContext db, MessageSender sender, SilentSender silent, AbilityBot bot) {
        this.db = db;
        this.sender = sender;
        this.silent = silent;
        this.budgetMap = db.getMap("BUDGET");
        this.expensesMap = db.getMap("EXPENSES");
        this.bot = bot;
    }

    @NotNull
    private Predicate<Update> hasMessageWith(String msg) {
        return upd -> upd.getMessage().getText().equalsIgnoreCase(msg);
    }

    private String getUserIdFromUpdate(Update update) {
        return String.valueOf(update.getMessage().getFrom().getId());
    }

    public ReplyFlow budgetFlow() {
        Reply saidRight = Reply.of(handleData(), upd -> upd.getMessage().hasText() && (upd.getMessage().getText()).split(" ").length >= 1);

        return ReplyFlow.builder(db)
                .action((bot, upd) -> {
                    Message msg = upd.getMessage();
                    String userIdString = String.valueOf(upd.getMessage().getFrom().getId());
                    silent.send("Please specify your budget!", upd.getMessage().getChatId());
                })
                .onlyIf(hasMessageWith("/budget"))
                .next(saidRight)
                .build();
    }

    public BiConsumer<BaseAbilityBot, Update> handleData() {
        return (bot, upd) -> {
            String budget = upd.getMessage().getText();
            Message msg = upd.getMessage();
            String userId = getUserIdFromUpdate(upd);
            List<Budget> userBudget = budgetMap.getOrDefault(userId, new LinkedList<>());

            userBudget.add(new Budget(Long.parseLong(budget)));

            silent.send(String.format("This month budget set for %s", budget), msg.getChatId());

            budgetMap.put(userId, userBudget);
        };
    }
}
