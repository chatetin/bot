package org.chatetin.handlers;

import org.chatetin.budget.Budget;
import org.chatetin.expense.ImageOCR;
import org.chatetin.model.Expense;
import org.telegram.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.objects.ReplyFlow;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class ExpenseHandler implements AbilityExtension {
    // DB and sender
    protected final DBContext db;
    protected MessageSender sender;
    protected SilentSender silent;
    private ImageOCR imageOCR;
    private Map<String, List<Expense>> expensesMap;
    private Map<String, List<Budget>> budgetMap;


    public ExpenseHandler(DBContext db, MessageSender sender, SilentSender silent,
                          ImageOCR imageOCR) {
        this.db = db;
        this.sender = sender;
        this.silent = silent;
        this.imageOCR = imageOCR;
        this.expensesMap = db.getMap("EXPENSES");
        this.budgetMap = db.getMap("BUDGET");
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private static Expense getLatestExpense(List<Expense> userExpenses) {
        return userExpenses.get(userExpenses.size() - 1);
    }

    private String getUserIdFromUpdate(Update update) {
        return String.valueOf(update.getMessage().getFrom().getId());
    }

    public Boolean isValidTextResponseToImage(Update update) {
        if (!update.getMessage().hasText()) return false;
        String text = update.getMessage().getText();
        String[] splitted = text.split(" ");
        return splitted.length == 2 || splitted.length == 3;
    }

    public ReplyFlow addFlow() {
//        Reply saidTextThenImage = Reply.of(handleImageTextData(),
//                upd -> upd.getMessage().hasText() && ((upd.getMessage().getText()).split(" ")
//                .length == 2 || (upd.getMessage().getText()).split(" ").length == 3));

        ReplyFlow saidTextThenImage = ReplyFlow.builder(db)
                .action(handleImageTextData())
                .onlyIf(this::isValidTextResponseToImage)
                .build();

        ReplyFlow saidImage = ReplyFlow.builder(db)
                .action(handleImageData())
                .onlyIf(upd -> upd.getMessage().hasPhoto())
                .next(saidTextThenImage).build();

//        Reply saidRight = Reply
//                .of(handleTextData(),
//                        upd -> upd.getMessage().hasText() && (upd.getMessage().getText()).split(
//                        " ").length >= 3);

        ReplyFlow saidText = ReplyFlow.builder(db)
                .action(handleTextData())
                .onlyIf(upd -> upd.getMessage().hasText() && (upd.getMessage().getText()).split(
                        " ").length >= 3)
                .build();

        return ReplyFlow.builder(db)
                .action((bot, upd) -> silent.send("Please specify your expenses or send a photo!"
                        , upd.getMessage().getChatId()))
                .onlyIf(hasMessageWith("/add"))
                .next(saidImage)
                .next(saidText)
                .build();
    }

    public BiConsumer<BaseAbilityBot, Update> handleImageTextData() {
        return (bot, upd) -> {
            Message msg = upd.getMessage();
            String userId = getUserIdFromUpdate(upd);
            List<Expense> userExpenses = expensesMap.getOrDefault(userId, new LinkedList<>());
            Expense latestExpense = getLatestExpense(userExpenses);

            String[] ms = upd.getMessage().getText().split(" ", 3);
            if (ms.length < 2 || (ms.length == 3 && !isNumeric(ms[0]))) {
                // input is invalid
                silent.send("Invalid input! Please re-input your image and data!", msg.getChatId());
            } else {
                if (isNumeric(ms[0])) { // value overridden
                    latestExpense.setValue(Long.parseLong(ms[0]));
                    latestExpense.setNotes(ms[1]);
                    latestExpense.setCategory(ms[2]);
                } else {
                    latestExpense.setNotes(ms[0]);
                    latestExpense.setCategory(ms[1]);
                }
                silent.send(String.format("Added %s!", latestExpense.getNotes()), msg.getChatId());
                expensesMap.put(userId, userExpenses);
            }
            try {
                long max = 0;
                for (Budget bud : budgetMap.get(userId)) {
                    max = bud.getMaxExpense();
                }

                int total = 0;
                for (Expense expense : expensesMap.get(userId)) {
                    total += expense.getValue();
                }

                long nine = (long) (0.9*max);

                if (total >= max) {
                    silent.send("You have exceeded your budget!\n" + "Your expense is " + total + " and your budget is " + max, msg.getChatId());
                } else if (total >= nine) {
                    silent.send("You have used 90% or more of your allocated budget!\n" + "Your expense is " + total + " and your budget is " + max, msg.getChatId());
                }
            } catch (NullPointerException npe) {
                return;
            }
        };
    }

    public void handleCompleteImageOCR(Optional<Long> value, Long chatId, String userId,
                                       List<Expense> userExpenses) {
        if (value.isPresent() && value.get() != -1L) {
            silent.send("Your Receipt Parsed Value: " + value.get(), chatId);
        } else {
            silent.send("Failed to parse your receipt!", chatId);
        }
        userExpenses.add(new Expense(value.orElse(-1L)));
        expensesMap.put(userId, userExpenses);

        silent.send("You can now either complete or override the expense data.\n" +
                        "'[Value] [Description] [Category]' or just '[Description] " +
                        "[Category]'",
                chatId);
    }

    public BiConsumer<BaseAbilityBot, Update> handleImageData() {
        return (bot, upd) -> {
            Message msg = upd.getMessage();
            String userId = getUserIdFromUpdate(upd);
            List<Expense> userExpenses = expensesMap.getOrDefault(userId, new LinkedList<>());

            CompletableFuture<Optional<Long>> cf =
                    imageOCR.getValueFromOCR(msg).map(f -> f.thenApply(Optional::of)).orElseGet(() -> CompletableFuture.completedFuture(Optional.empty()));
            cf.whenComplete((value, ex) -> {
                handleCompleteImageOCR(value, msg.getChatId(), userId, userExpenses);
//                if (value.isPresent() && value.get() != -1L) {
//                    silent.send("Your Receipt Parsed Value: " + value.get(), msg.getChatId());
//                } else {
//                    silent.send("Failed to parse your receipt!", msg.getChatId());
//                }
//                userExpenses.add(new Expense(value.orElse(-1L)));
//                expensesMap.put(userId, userExpenses);
//
//                silent.send("You can now either complete or override the expense data.\n" +
//                                "'[Value] [Description] [Category]' or just '[Description] " +
//                                "[Category]'",
//                        msg.getChatId());
            });
            try {
                long max = 0;
                for (Budget bud : budgetMap.get(userId)) {
                    max = bud.getMaxExpense();
                }

                int total = 0;
                for (Expense expense : expensesMap.get(userId)) {
                    total += expense.getValue();
                }

                long nine = (long) (0.9*max);

                if (total >= max) {
                    silent.send("You have exceeded your budget!\n" + "Your expense is " + total + " and your budget is " + max, msg.getChatId());
                } else if (total >= nine) {
                    silent.send("You have used 90% or more of your allocated budget!\n" + "Your expense is " + total + " and your budget is " + max, msg.getChatId());
                }
            } catch (NullPointerException npe) {
                return;
            }
        };
    }

    public BiConsumer<BaseAbilityBot, Update> handleTextData() {
        return (bot, upd) -> {
            String[] messages = upd.getMessage().getText().split("\n");
            Message msg = upd.getMessage();
            String userId = getUserIdFromUpdate(upd);
            List<Expense> userExpenses = expensesMap.getOrDefault(userId, new LinkedList<>());

            for (String message : messages) {
                String[] ms = message.split(" ", 3);
                userExpenses.add(new Expense(Long.parseLong(ms[0]), ms[1], ms[2]));
                silent.send(String.format("Added %s!", ms[1]), msg.getChatId());
            }

            expensesMap.put(userId, userExpenses);

            try {
                long max = 0;
                for (Budget bud : budgetMap.get(userId)) {
                    max = bud.getMaxExpense();
                }

                int total = 0;
                for (Expense expense : expensesMap.get(userId)) {
                    total += expense.getValue();
                }

                long nine = (long) (0.9*max);

                if (total >= max) {
                    silent.send("You have exceeded your budget!\n" + "Your expense is " + total + " and your budget is " + max, msg.getChatId());
                } else if (total >= nine) {
                    silent.send("You have used 90% or more of your allocated budget!\n" + "Your expense is " + total + " and your budget is " + max, msg.getChatId());
                }
            } catch (NullPointerException npe) {
                return;
            }


        };
    }

    @NotNull
    private Predicate<Update> hasMessageWith(String msg) {
        return upd -> upd.getMessage().getText().equalsIgnoreCase(msg);
    }
}
