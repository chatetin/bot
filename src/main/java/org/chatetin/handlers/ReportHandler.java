package org.chatetin.handlers;

import org.chatetin.model.Expense;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.objects.*;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class ReportHandler implements AbilityExtension{
    protected final DBContext db;
    protected MessageSender sender;
    protected SilentSender silent;
    private Map<String, List<Expense>> expensesMap;
    protected  AbilityBot bot;

    public ReportHandler(DBContext db, MessageSender sender, SilentSender silent, AbilityBot bot){
        this.db = db;
        this.sender = sender;
        this.silent = silent;
        this.expensesMap = db.getMap("EXPENSES");
        this.bot = bot;
    }

    private Predicate<Update> hasMessageWith(String msg) {
        return upd -> upd.getMessage().getText().equalsIgnoreCase(msg);
    }

    public ReplyFlow replyToExport() {

        Reply getFilter = Reply
                .of(handleReportFilter(), upd -> upd.getMessage().hasText() && !(upd.getMessage().getText()).equals("/report"));


        return ReplyFlow
                .builder(db)
                .action((bot, upd) ->{
                    Map<String, List<Expense>> expensesMap = db.getMap("EXPENSES");
                    Message msg = upd.getMessage();
                    String userIdString = String.valueOf(upd.getMessage().getFrom().getId());;

                    if (expensesMap.containsKey(userIdString)) {
                        silent.send("Choose filter for your report expense: \n" +
                                            "Category \n example: 'makan', 'Hiburan' \n" + "Month \n example: 'january' \n" + "Day \n example: 'monday' \n ", upd.getMessage().getChatId());
                        ArrayList<String> sudahTerpanggil = new ArrayList<String>();
                        silent.send("This is the category available in your report if you want to choose filter by Category: \n" , upd.getMessage().getChatId());
                        for (Expense expense : expensesMap.get(userIdString)){
                            if(sudahTerpanggil.contains(expense.getCategory()) == false) {
                                sudahTerpanggil.add(expense.getCategory());
                            }
                        }
                        for (int i = 0 ; i<sudahTerpanggil.size(); i++) {

                            silent.send( sudahTerpanggil.get(i) + "\n" , upd.getMessage().getChatId());
                        }
                    }else {
                        silent.send("Report is Empty! \n Please add the report first with command /add", upd.getMessage().getChatId());
                    }

                })
                .onlyIf(hasMessageWith("/report"))
                .next(getFilter)
                .build();
    }

    public BiConsumer<BaseAbilityBot, Update> handleReportFilter(){

        return ((bot, upd) -> {
            Map<String, List<Expense>> expensesMap = db.getMap("EXPENSES");
            Message msg = upd.getMessage();
            String userIdString = String.valueOf(upd.getMessage().getFrom().getId());
            List<Expense> userExpenses = expensesMap.getOrDefault(userIdString, new LinkedList<>());

            System.out.println(upd.getMessage().getText());

            String filter = upd.getMessage().getText().toLowerCase(Locale.ROOT);
            String[] monthNames = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
            String[] dayNames = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"};
            ArrayList<String> categoryNames = new ArrayList<String>();

            if(expensesMap.get(userIdString)!=null) {
                for (Expense expense : expensesMap.get(userIdString)) {
                    categoryNames.add(expense.getCategory().toLowerCase(Locale.ROOT));
                }
            }

            boolean isMonth = false;
            boolean isDay = false;
            boolean isCategory = false;

            for(String valid : monthNames){
                if(filter.equals(valid)){
                    isMonth = true;
                    break;
                }
            }for(String valid : dayNames){
                if(filter.equals(valid)){
                    isDay = true;
                    break;
                }
            }for(String valid : categoryNames){
                if(filter.equals(valid)){
                    isCategory = true;
                    break;
                }
            }

            int total = 0;
            if(isMonth) {
                silent.send("Pengeluaran untuk bulan " + filter, upd.getMessage().getChatId());
                if(expensesMap.get(userIdString)!=null) {
                    for (Expense expense : expensesMap.get(userIdString)) {
                        if (expense.getMonth().equals(filter)) {
                            silent.send(expense.toString(), upd.getMessage().getChatId());
                            total += expense.getValue();
                        }
                    }
                }
                silent.send("Total - Rp. " + total, upd.getMessage().getChatId());
            }else if(isCategory){
                silent.send("Pengeluaran untuk " + filter, upd.getMessage().getChatId());
                if(expensesMap.get(userIdString)!=null) {
                    for (Expense expense : expensesMap.get(userIdString)) {
                        if (expense.getCategory().equals(filter)) {
                            silent.send(expense.toString(), upd.getMessage().getChatId());
                            total += expense.getValue();
                        }
                    }
                }
                silent.send("Total - Rp. " + total, upd.getMessage().getChatId());
            }else if(isDay){
                silent.send("your expenses on " + filter, upd.getMessage().getChatId());
                if(expensesMap.get(userIdString)!=null) {
                    for (Expense expense : expensesMap.get(userIdString)) {
                        if (expense.getDay().toLowerCase(Locale.ROOT).equals(filter)) {
                            silent.send(expense.toString(), upd.getMessage().getChatId());
                            total += expense.getValue();
                        }
                    }
                }
                silent.send("Total - Rp. " + total, upd.getMessage().getChatId());
            }
            else {
                silent.send(String.format("%s invalid filter!",filter),upd.getMessage().getChatId());
            }
        });
    }
}
