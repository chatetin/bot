package org.chatetin.handlers;

import org.chatetin.payment.Constants;
import org.chatetin.payment.Payment;
import org.telegram.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.objects.Reply;
import org.telegram.abilitybots.api.objects.ReplyFlow;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class PaymentHandler implements AbilityExtension {
    
    // DB and sender
    protected final DBContext db;
    protected MessageSender sender;
    protected SilentSender silent;
    private Map<String, List<Payment>> paymentMap;
    private Map<String, List<String[]>> recurringMap;

    public PaymentHandler(DBContext db, MessageSender sender, SilentSender silent) {
        this.db = db;
        this.sender = sender;
        this.silent = silent;
        this.paymentMap = db.getMap("PAYMENT");
        this.recurringMap = db.getMap("RECURRING");
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    //Java 8 - Use DateTimeFormatter (thread-safe)
    public static boolean isValidDate(String dateStr)
    {
        if (dateStr == null) {
            return false;
        }
        LocalDate date = null;
        try {
            date = LocalDate.parse(dateStr);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }


    private String getUserIdFromUpdate(Update update) {
        return String.valueOf(update.getMessage().getFrom().getId());
    }

    public ReplyFlow addReminder() {
        Reply saidRight = Reply.of(handleTextDataReminder(),
                upd -> upd.getMessage().hasText() && (upd.getMessage().getText()).split(" ").length >= 4);

        return ReplyFlow.builder(db)
                .action((bot, upd) -> silent.send("Please specify your payment! [Value] [Description] [Category] [Date (format: YYYY-MM-DD)] Ex: 500000 Stock Investment 2021-12-12", upd.getMessage().getChatId()))
                .onlyIf(hasMessageWith("/addReminder"))
                .next(saidRight)
                .build();
    }

    public BiConsumer<BaseAbilityBot, Update> handleTextDataReminder() {
        return (bot, upd) -> {
            String[] messages = upd.getMessage().getText().split("\n");
            Message msg = upd.getMessage();
            String userId = getUserIdFromUpdate(upd);
            List<Payment> userPayments = paymentMap.getOrDefault(userId, new LinkedList<>());

            for (String message : messages) {
                String[] ms = message.split(" ", 4);
                System.out.println(message);
                if (!isNumeric(ms[0]) || !isValidDate(ms[3])) {
                    // input is invalid
                    silent.send("Invalid input! Please re-input your payment information!", msg.getChatId());
                } else if (LocalDate.parse(ms[3]).compareTo(LocalDate.now()) < 0) {
                    //due date already passed
                    silent.send(String.format("Your due date is already passed"), msg.getChatId());
                } else {
                    userPayments.add(new Payment(Long.parseLong(ms[0]), ms[1], ms[2], LocalDate.parse(ms[3])));
                    silent.send(String.format("Added %s!", ms[1]), msg.getChatId());
                    Collections.sort(userPayments, new Comparator<>() {
                        @Override
                        public int compare(Payment o1, Payment o2) {
                            return o1.getDue().compareTo(o2.getDue());
                        }
                    });
                    paymentMap.put(userId, userPayments);
                }
            }
        };
    }

    public ReplyFlow addRecurring() {
        Reply saidRight = Reply.of(handleTextDataRecurring(),
                upd -> upd.getMessage().hasText() && (upd.getMessage().getText()).split(" ").length >= 4);

        return ReplyFlow.builder(db)
                .action((bot, upd) -> silent.send("Please specify your recurring payment! [Value] [Description] [Category] [Date (format: DD)] Ex: 500000 Stock Investment 12 -> We will remind you every month on this day", upd.getMessage().getChatId()))
                .onlyIf(hasMessageWith("/addRecurring"))
                .next(saidRight)
                .build();
    }

    public BiConsumer<BaseAbilityBot, Update> handleTextDataRecurring() {
        return (bot, upd) -> {
            String[] messages = upd.getMessage().getText().split("\n");
            Message msg = upd.getMessage();
            String userId = getUserIdFromUpdate(upd);

            List<String[]> userRecurrings = recurringMap.getOrDefault(userId, new LinkedList<>());
            List<Payment> userPayments = paymentMap.getOrDefault(userId, new LinkedList<>());

            for (String message : messages) {
                String[] ms = message.split(" ", 4);

                if (!isNumeric(ms[0]) || !isNumeric(ms[3])) {
                    // input is invalid
                    silent.send("Invalid input! Please re-input your payment information!", msg.getChatId());
                }
                else{
                    userRecurrings.add(ms);
                    Collections.sort(userRecurrings, new Comparator<>() {
                        @Override
                        public int compare(String[] o1, String[] o2) {
                            return o1[3].compareTo(o2[3]);
                        }
                    });
                    recurringMap.put(userId, userRecurrings);
                    silent.send(String.format("Added %s!", ms[1]), msg.getChatId());

                    if(Integer.parseInt(ms[3])>LocalDate.now().getDayOfMonth()){
                        LocalDate due = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), Integer.parseInt(ms[3]));
                        System.out.println(due);
                        userPayments.add(new Payment(Long.parseLong(ms[0]), ms[1], ms[2], due));
                        Collections.sort(userPayments, new Comparator<>() {
                            @Override
                            public int compare(Payment o1, Payment o2) {
                                return o1.getDue().compareTo(o2.getDue());
                            }
                        });
                        paymentMap.put(userId, userPayments);
                    }
                }
            }

        };
    }

    public ReplyFlow stopRecurring() {
        Reply saidRight = Reply.of(handleTextDataStopRecurring(),
                upd -> upd.getMessage().hasText());

        return ReplyFlow.builder(db)
                .action((bot, upd) -> {
                    if(recurringMap.get(getUserIdFromUpdate(upd))!=null && recurringMap.get(getUserIdFromUpdate(upd)).size()>0){
                        String textReply = "Please choose which number of your recurring payment below you want to delete!"+"\r\n";
                        int idx = 1;
                        for(String[] recurring : recurringMap.get(getUserIdFromUpdate(upd))){
                            textReply+= idx+". [Date "+recurring[3]+"] "+recurring[1]+ ", "+recurring[2]+", "+recurring[0]+"\r\n";
                            idx++;
                        }
                        silent.send(textReply, upd.getMessage().getChatId());
                    }
                    else{
                        silent.send("You don't have any recurring payment right now", upd.getMessage().getChatId());
                    }
                })
                .onlyIf(hasMessageWith("/stopRecurring"))
                .next(saidRight)
                .build();
    }

    public BiConsumer<BaseAbilityBot, Update> handleTextDataStopRecurring() {
        return (bot, upd) -> {
            Message msg = upd.getMessage();
            String userId = getUserIdFromUpdate(upd);
            List<String[]> userRecurrings = recurringMap.getOrDefault(userId, new LinkedList<>());

            if(Integer.parseInt(msg.getText())<=userRecurrings.size()){
                userRecurrings.remove(Integer.parseInt(msg.getText())-1);
                recurringMap.put(userId, userRecurrings);
                silent.send("Deleted! We won't remind you about this payment start from next month", msg.getChatId());
            }
            else {
                silent.send("Invalid input! Please re-input your payment information!", msg.getChatId());
            }
        };
    }

    @NotNull
    private Predicate<Update> hasMessageWith(String msg) {
        return upd -> upd.getMessage().getText().equalsIgnoreCase(msg);
    }

    public void sayReminder() {
        try {
            processReminder();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void processReminder() throws TelegramApiException {
        System.out.println("Payment reminder execute");
        for(Map.Entry<String, List<Payment>> entry: paymentMap.entrySet()){
            if(checkPayment(entry.getKey())){
                SendMessage sm = new SendMessage();
                sm.setText(Constants.TEXT_REMINDER);
                sm.setChatId(String.valueOf(entry.getKey()));
                sender.execute(sm);
            }
        }
    }

    public void updatePaymentMap() {
        try {
            doUpdate();
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void doUpdate() throws TelegramApiException {
        System.out.println("Update Payment List");
        for(Map.Entry<String, List<Payment>> entry: paymentMap.entrySet()){
            List<String[]> userRecurrings = recurringMap.get(entry.getKey());
            List<Payment> userPayments = paymentMap.getOrDefault(entry.getKey(), new LinkedList<>());
            List<Payment> newPaymentsList = new LinkedList<>();

            for(Payment pay : userPayments) {
                if (pay.getDue().compareTo(LocalDate.now()) >= 0) {
                    newPaymentsList.add(pay);
                }
            }

            for (String[] userRecurring : userRecurrings) {
                LocalDate due = LocalDate.of(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), Integer.parseInt(userRecurring[3]));
                newPaymentsList.add(new Payment(Long.parseLong(userRecurring[0]), userRecurring[1], userRecurring[2], due));
            }

            Collections.sort(newPaymentsList, new Comparator<>(){
                @Override
                public int compare(Payment o1, Payment o2) {
                    return o1.getDue().compareTo(o2.getDue());
                }
            });

            paymentMap.replace(entry.getKey(),newPaymentsList);
            SendMessage sm = new SendMessage();
            sm.setText(Constants.TEXT_UPDATE);
            sm.setChatId(String.valueOf(entry.getKey()));
            sender.execute(sm);
        }
    }

    public boolean checkPayment(String chatId){
        for(Payment reccuring : paymentMap.get(chatId)){
            if(reccuring.getDue().compareTo(LocalDate.now())==0){
                return true;
            }
        }
        return false;
    }
}