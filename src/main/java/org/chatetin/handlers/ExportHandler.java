package org.chatetin.handlers;

import org.chatetin.export.CSVExporter;
import org.chatetin.model.Expense;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.abilitybots.api.db.DBContext;
import org.telegram.abilitybots.api.objects.*;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.abilitybots.api.objects.ReplyFlow;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class ExportHandler implements AbilityExtension{
    protected final DBContext db;
    protected MessageSender sender;
    protected SilentSender silent;
    private Map<String, List<Expense>> expensesMap;
    protected  AbilityBot bot;

    public ExportHandler(DBContext db, MessageSender sender, SilentSender silent, AbilityBot bot){
        this.db = db;
        this.sender = sender;
        this.silent = silent;
        this.expensesMap = db.getMap("EXPENSES");
        this.bot = bot;
    }

    private Predicate<Update> hasMessageWith(String msg) {
        return upd -> upd.getMessage().getText().equalsIgnoreCase(msg);
    }

    public ReplyFlow replyToExport() {
        Reply exportCSV = ReplyFlow.builder(db)
                .build();

//        Reply getmonth = Reply
//                .of(handleExportperMonth(),upd -> upd.getMessage().hasText() && !(upd.getMessage().getText()).equals("/export") && (upd.getMessage().getText()).split(" ").length == 1);
        ReplyFlow getmonth = ReplyFlow.builder(db)
                .action(handleExportperMonth())
                .onlyIf(upd -> upd.getMessage().hasText() && !(upd.getMessage().getText()).equals("/export") && (upd.getMessage().getText()).split(" ").length == 1)
                .build();

        return ReplyFlow.builder(db)
                .action((bot, upd) -> silent.send("Please specify your expenses month report", upd.getMessage().getChatId()))
                .onlyIf(hasMessageWith("/export"))
                .next(getmonth)
                .build();
    }

    public BiConsumer<BaseAbilityBot, Update> handleExportperMonth(){

        return ((bot, upd) -> {
            Message msg = upd.getMessage();
            String userId = String.valueOf(upd.getMessage().getFrom().getId());;
            List<Expense> userExpenses = expensesMap.getOrDefault(userId, new LinkedList<>());
            //Expense latestExpense = userExpenses.get(userExpenses.size() - 1);

            System.out.println(upd.getMessage().getText());
            Map<String, List<Expense>> expensesMap = db.getMap("EXPENSES");

            String month = upd.getMessage().getText().toLowerCase(Locale.ROOT);
            String[] monthNames = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
            boolean next = false;
            for(String valid : monthNames){
                if(month.equals(valid)){
                    next = true;
                    break;
                }
            }
            if(next) {
                CSVExporter csvExporter = new CSVExporter(userExpenses,month);
                CompletableFuture<Void> csvE = CompletableFuture.runAsync(csvExporter::convertToCSV);
                File file = new File(String.format("Report_%s.csv",month));
                csvE.whenComplete((value, ex) -> {
                    sendDocFile(file.getAbsolutePath(),userId);});
                silent.send(String.format("expenses in %s!", month), upd.getMessage().getChatId());
            } else {
                silent.send(String.format("%s invalid month!",month),upd.getMessage().getChatId());
            }
        });
    }

    public void sendDocFile(String filepath, String chatId){
        // Create send method
        SendDocument sendDocumentRequest = new SendDocument();
        // Set destination chat id
        sendDocumentRequest.setChatId(chatId);
        // Set the doc file as a new doc
        sendDocumentRequest.setDocument(new InputFile(new File(filepath)));
        try {
            bot.execute(sendDocumentRequest); } catch (TelegramApiException e) { e.printStackTrace(); }
    }

}
