package org.chatetin.payment;

public interface Constants {
    String TEXT_REMINDER = "Hello! Don't forget you have a due payment, today. To check your due payment, use command /reminder";
    String TEXT_UPDATE = "Hello! We just update the list of your payment reminder this month. To check your due payment, use command /reminder";
}
