package org.chatetin.payment.reminder;

public interface Reminder {
    void executeReminder();
    void executeUpdate();
}
