package org.chatetin.payment.reminder;

public class ReminderImpl implements Reminder{
    private final Callback callback;

    public interface Callback {
        void onTimeForReminder();
        void onTimeForUpdate();
    }

    public ReminderImpl(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void executeReminder() {
        callback.onTimeForReminder();
    }

    @Override
    public void executeUpdate() {
        callback.onTimeForUpdate();
    }
}
