package org.chatetin.payment;

import java.io.Serializable;
import java.time.LocalDate;;

public class Payment implements Serializable {
    private long value;
    private String notes;
    private String category;
    private LocalDate due;

    public Payment(long value, String notes, String category, LocalDate due) {
        this.value = value;
        this.notes = notes;
        this.category = category;
        this.due = due;
    }

    public Payment(){};

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LocalDate getDue() {
        return due;
    }

    public void setDue(LocalDate due) {
        this.due = due;
    }
}
