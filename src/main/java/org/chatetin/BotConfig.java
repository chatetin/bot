package org.chatetin;

public class BotConfig {
     public static final String BOT_USERNAME = "Test_chatetin_expense_report_bot"; //mario
     public static final Boolean IS_PRODUCTION = System.getenv("PRODUCTION") != null && System.getenv("PRODUCTION").equalsIgnoreCase("true");

     public static String getBotToken() {
          return System.getenv("BOT_TOKEN");
     }

     public static String getOCRAPIUrl() {
          return IS_PRODUCTION ? System.getenv("OCR_API_URL") : "https://ocr-api-upgylvqrwq-et.a.run.app/";
     }
}
